import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  preguntasRespuestas: any;

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    public router: Router) { 

      
    }

  ngOnInit() {
   
  }

  ionViewWillEnter() {
    this.getPreguntaRespuestaAll();
  }

  async getPreguntaRespuestaAll() {
    const loading = await this.loadingController.create({
      content: 'Loading'
    });
    await loading.present();
    await this.api.getPreguntayRespuesta()
      .subscribe(res => {
        console.log(res);
        this.preguntasRespuestas = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  showDetail(id) {
    this.router.navigate(['/detail', JSON.stringify(id)]);
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
