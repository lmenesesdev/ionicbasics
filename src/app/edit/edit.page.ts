import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  PreguntaRespuestaForm: FormGroup;
  students: FormArray;

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
    this.preguntayRespuestaById(this.route.snapshot.paramMap.get('id'));
    this.PreguntaRespuestaForm = this.formBuilder.group({
      'pregunta' : [null, Validators.required],
      'respuesta' : [null, Validators.required],
    });
  }

  ngOnInit() {
  }

  async preguntayRespuestaById(id) {
    const loading = await this.loadingController.create({
      content: 'Loading'
    });
    await loading.present();
    await this.api.getPreguntayRespuestaById(id).subscribe(res => {
      this.PreguntaRespuestaForm.controls['pregunta'].setValue(res.pregunta);
      this.PreguntaRespuestaForm.controls['respuesta'].setValue(res.respuesta);
       
      console.log(this.PreguntaRespuestaForm);
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
  }

  async updatePyR(){
    await this.api.updatePreguntayRespuesta(this.route.snapshot.paramMap.get('id'), this.PreguntaRespuestaForm.value)
    .subscribe(res => {
        //let id = res['id'];
        //this.router.navigate(['/detail/'+id]);
        this.router.navigate(['/list/']);
      }, (err) => {
        console.log(err);
      });
  }

}
