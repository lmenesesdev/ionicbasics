import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  preguntaRespuesta: any = {};

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.getPreguntaRespuesta();
  }

  async getPreguntaRespuesta() {
    const loading = await this.loadingController.create({
      content: 'Loading'
    });
    await loading.present();
    await this.api.getPreguntayRespuestaById(this.route.snapshot.paramMap.get('id'))
      .subscribe(res => {
        console.log(res);
        this.preguntaRespuesta = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async delete(id) {
    const loading = await this.loadingController.create({
      content: 'Deleting'
    });
    await loading.present();
    await this.api.deletePreguntayRespuesta(id)
      .subscribe(res => {

        console.log(`Delete ${res}`);
        loading.dismiss();
        this.router.navigate(['/list']);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  edit(id) {
    this.router.navigate(['/edit', JSON.stringify(id)]);
  }

}
