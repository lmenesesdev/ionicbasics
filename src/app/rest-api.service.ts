import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

const apiUrl = "https://apistefaniniecopetrol.azurewebsites.net/api";


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http: HttpClient) { 

    let loginModel = {
      userId: "luis",
      password: "luis"
    };
  
    this.login(loginModel)
      .subscribe(arg => window.localStorage.setItem("token", arg.token));       
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }



  login(login): Observable<any> {
    
      return this.http.post(`${apiUrl}/Login`, login, { headers: new HttpHeaders({'Content-Type': 'application/json'}) })
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
  }

  getPreguntayRespuesta(): Observable<any> {
      
    return this.http.get(`${apiUrl}/PreguntaRespuesta/GetAll`, this.getToken()).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getPreguntayRespuestaById(id: string): Observable<any> {
    const url = `${apiUrl}/PreguntaRespuesta/GetById?id=${id}`;
    return this.http.get(url, this.getToken()).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  postPreguntayRespuesta(data): Observable<any> {

    console.log(data);

    const url = `${apiUrl}/PreguntaRespuesta/Add`;
    return this.http.post(url, data, this.getToken())
      .pipe(
        catchError(this.handleError)
      );
  }

  updatePreguntayRespuesta(id:string, data): Observable<any> {
    const url = `${apiUrl}/PreguntaRespuesta/Update`;
    data.idpyr = id;
    
    return this.http.put(url, data, this.getToken())
      .pipe(
        catchError(this.handleError)
      );
  }

  deletePreguntayRespuesta(id: string): Observable<{}> {
    const url = `${apiUrl}/PreguntaRespuesta/Delete?id=${id}`;
    return this.http.delete(url, this.getToken())
      .pipe(
        catchError(this.handleError)
      );
  }


  private getToken(){

    let token = window.localStorage.getItem('token');

    return {
      headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`})
    }
  }

  
}
