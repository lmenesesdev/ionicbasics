import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  PreguntaRespuestaForm: FormGroup;

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.PreguntaRespuestaForm = this.formBuilder.group({
        'pregunta' : [null, Validators.required],
        'respuesta' : [null, Validators.required],    
      });
    }

  ngOnInit() {

    
  }


  async createPyR(){

    console.log('antes delete');
     console.log(this.PreguntaRespuestaForm.value);

    await this.api.postPreguntayRespuesta(this.PreguntaRespuestaForm.value)
    .subscribe(res => {
        //let id = res['id'];
        //his.router.navigate(['/detail/'+id]);
        this.router.navigate(['/list']);
      }, (err) => {
        console.log(err);
      });
  }

}
